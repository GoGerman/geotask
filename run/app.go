package run

import (
	"context"
	"github.com/gin-gonic/gin"
	"gitlab.com/GoGerman/geotask/cache"
	"gitlab.com/GoGerman/geotask/geo"
	cservice "gitlab.com/GoGerman/geotask/module/courier/service"
	cstorage "gitlab.com/GoGerman/geotask/module/courier/storage"
	"gitlab.com/GoGerman/geotask/module/courierfacade/controller"
	cfservice "gitlab.com/GoGerman/geotask/module/courierfacade/service"
	oservice "gitlab.com/GoGerman/geotask/module/order/service"
	ostorage "gitlab.com/GoGerman/geotask/module/order/storage"
	"gitlab.com/GoGerman/geotask/router"
	"gitlab.com/GoGerman/geotask/server"
	"gitlab.com/GoGerman/geotask/workers/order"
	"net/http"
	"os"
	"time"
)

type App struct {
}

func NewApp() *App {
	return &App{}
}

func (a *App) Run() error {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Millisecond)
	defer cancel()
	// получение хоста и порта redis
	host := os.Getenv("REDIS_HOST")
	port := os.Getenv("REDIS_PORT")
	// инициализация клиента redis
	rclient := cache.NewRedisClient(host, port)
	// проверка доступности redis
	_, err := rclient.Ping(ctx).Result()
	if err != nil {
		return err
	}

	rclient.FlushAll(context.Background())
	// инициализация разрешенной зоны
	allowedZone := geo.NewAllowedZone()
	// инициализация запрещенных зон
	disAllowedZones := []geo.PolygonChecker{
		geo.NewDisAllowedZone1(),
		geo.NewDisAllowedZone2(),
	}
	// инициализация хранилища заказов
	orderStorage := ostorage.NewOrderStorage(rclient)
	// инициализация сервиса заказов
	orderService := oservice.NewOrderService(orderStorage, allowedZone, disAllowedZones)

	orderGenerator := order.NewOrderGenerator(orderService)
	orderGenerator.Run()

	oldOrderCleaner := order.NewOrderCleaner(orderService)
	oldOrderCleaner.Run()

	// инициализация хранилища курьеров
	courierStorage := cstorage.NewCourierStorage(rclient)

	// инициализация сервиса курьеров
	courierService := cservice.NewCourierService(courierStorage, allowedZone, disAllowedZones)

	// инициализация фасада сервиса курьеров
	courierFacade := cfservice.NewCourierFacade(courierService, orderService)

	// инициализация контроллера курьеров
	courierController := controller.NewCourierController(courierFacade)

	// инициализация роутера
	routes := router.NewRouter(courierController)

	//gin.SetMode(gin.ReleaseMode)
	// инициализация сервера
	r := server.NewHTTPServer()
	// инициализация группы роутов
	api := r.Group("/api")
	// инициализация роутов
	routes.CourierAPI(api)

	mainRoute := r.Group("/")

	routes.Swagger(mainRoute)
	// инициализация статических файлов
	r.NoRoute(gin.WrapH(http.FileServer(http.Dir("public"))))

	// запуск сервера
	//serverPort := os.Getenv("SERVER_PORT")

	if os.Getenv("ENV") == "prod" {
		certFile := "/app/certs/cert.pem"
		keyFile := "/app/certs/private.pem"
		return r.RunTLS(":443", certFile, keyFile)
	}

	return r.Run()
}
