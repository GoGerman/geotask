package geo

import (
	geo "github.com/kellydunn/golang-geo"
	"github.com/robertkrimen/otto"
	"log"
	"math/rand"
	"os"
	"strings"
	"time"
)

type Point struct {
	Lat float64 `json:"lat"`
	Lng float64 `json:"lng"`
}

//go:generate mockery --name PolygonChecker

type PolygonChecker interface {
	Contains(point Point) bool // проверить, находится ли точка внутри полигона
	Allowed() bool             // разрешено ли входить в полигон
	RandomPoint() Point        // сгенерировать случайную точку внутри полигона
}

type Polygon struct {
	polygon *geo.Polygon
	allowed bool
}

func NewPolygon(points []Point, allowed bool) *Polygon {
	// используем библиотеку golang-geo для создания полигона
	var geoPoints []*geo.Point
	for _, p := range points {
		geoPoints = append(geoPoints, geo.NewPoint(p.Lat, p.Lng))
	}
	return &Polygon{
		polygon: geo.NewPolygon(geoPoints),
		allowed: allowed,
	}
}

func (p *Polygon) Contains(point Point) bool {
	return p.polygon.Contains(geo.NewPoint(point.Lat, point.Lng))
}

func (p *Polygon) Allowed() bool {
	return p.allowed
}

func (p *Polygon) RandomPoint() Point {
	rand.Seed(time.Now().UnixNano())

	minLat, maxLat := 59.81002495078666, 60.09897148869136
	minLng, maxLng := 30.14495968779295, 30.553621053301985
	for _, p := range p.polygon.Points() {
		if p.Lat() < minLat {
			minLat = p.Lat()
		}
		if p.Lat() > maxLat {
			maxLat = p.Lat()
		}
		if p.Lng() < minLng {
			minLng = p.Lng()
		}
		if p.Lng() > maxLng {
			maxLng = p.Lng()
		}
	}
	for {
		x := rand.Float64()*(maxLat-minLat) + minLat
		y := rand.Float64()*(maxLng-minLng) + minLng
		if p.Contains(Point{Lat: x, Lng: y}) {
			return Point{Lat: x, Lng: y}
		}
	}
}

func CheckPointIsAllowed(point Point, allowedZone PolygonChecker, disabledZones []PolygonChecker) bool {
	// проверить, находится ли точка в разрешенной зоне
	if allowedZone.Contains(point) {
		for i := range disabledZones {
			if disabledZones[i].Contains(point) {
				return false
			}
		}
		return true
	}
	return false
}

func GetRandomAllowedLocation(allowedZone PolygonChecker, disabledZones []PolygonChecker) Point {
	var point Point
	for {
		point = allowedZone.RandomPoint()
		if CheckPointIsAllowed(point, allowedZone, disabledZones) {
			break
		}
	}
	return point
}

func NewDisAllowedZone1() *Polygon {
	// добавить полигон с запрещенной зоной
	// полигоны лежат в /public/js/polygons.js

	points := ParsePoints(1)
	return NewPolygon(points, false)
}

func NewDisAllowedZone2() *Polygon {
	// добавить полигон с запрещенной зоной
	// полигоны лежат в /public/js/polygons.js

	points := ParsePoints(2)
	return NewPolygon(points, false)
}

func NewAllowedZone() *Polygon {
	// добавить полигон с разрешенной зоной
	// полигоны лежат в /public/js/polygons.js

	points := ParsePoints(0)
	return NewPolygon(points, true)
}

func ParsePoints(indx int) []Point {
	vm := otto.New()

	// Загрузка содержимого файла polygon.js в виртуальную машину JavaScript
	file, err := os.ReadFile("./public/js/polygon.js")
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}

	txt := strings.Split(string(file), ";")

	vars := []string{"mainPolygon", "noOrdersPolygon1", "noOrdersPolygon2"}

	_, err = vm.Run(txt[indx])
	if err != nil {
		log.Println(err)
		os.Exit(1)
	}
	// Извлечение значения переменной
	value, err := vm.Get(vars[indx])
	if err != nil {
		log.Println(err)
		os.Exit(2)
	}
	// Проверка типа значения
	if !value.IsObject() {
		log.Println(err)
		os.Exit(3)
	}
	// Преобразование значения в массив
	arrayValue, err := value.Export()
	if err != nil {
		log.Println(err)
		os.Exit(4)
	}
	polygon, ok := arrayValue.([][]float64)
	if !ok {
		log.Println(err)
		os.Exit(5)
	}

	points := make([]Point, len(polygon))
	for i := range polygon {
		points[i].Lat = polygon[i][0]
		points[i].Lng = polygon[i][1]
	}
	return points
}
