package docs

import "gitlab.com/GoGerman/geotask/module/courierfacade/models"

//go:generate swagger generate spec -o ../public/swagger.json --scan-models

// добавить документацию для роута /api/status

// swagger:route GET /api/status status GetStatusRequest
// Получение статуса сервиса.
// responses:
//   200: GetStatusResponse

// swagger:parameters GetStatusRequest
type GetListRequest struct {
}

// swagger:response GetStatusResponse
type GetListResponse struct {
	// in:body
	Body models.CourierStatus
}
