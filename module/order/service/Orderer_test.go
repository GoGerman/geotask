package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/GoGerman/geotask/geo"
	"gitlab.com/GoGerman/geotask/module/order/models"
	"gitlab.com/GoGerman/geotask/module/order/storage"
	"reflect"
	"testing"
)

func TestOrderer_GenerateOrder(t *testing.T) {
	type fields struct {
		storage       *storage.MockOrderStorager
		allowedZone   *geo.MockPolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				storage:       storage.NewMockOrderStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("GenerateUniqueID", mock.Anything).Return(int64(0), nil)
			tt.fields.storage.On("Save", tt.args.ctx, mock.Anything, mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)
			tt.fields.allowedZone.On("RandomPoint").Return(geo.Point{})

			_m := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := _m.GenerateOrder(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("GenerateOrder() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderer_GetByRadius(t *testing.T) {
	type fields struct {
		storage       *storage.MockOrderStorager
		allowedZone   *geo.MockPolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx    context.Context
		lng    float64
		lat    float64
		radius float64
		unit   string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    []models.Order
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				storage:       storage.NewMockOrderStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx:    context.Background(),
				lng:    0,
				lat:    0,
				radius: 0,
				unit:   "m",
			},
			want:    []models.Order{},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("GetByRadius", tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit).Return(tt.want, nil)

			_m := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			got, err := _m.GetByRadius(tt.args.ctx, tt.args.lng, tt.args.lat, tt.args.radius, tt.args.unit)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetByRadius() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetByRadius() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderer_GetCount(t *testing.T) {
	type fields struct {
		storage       *storage.MockOrderStorager
		allowedZone   *geo.MockPolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    int
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				storage:       storage.NewMockOrderStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx: context.Background(),
			},
			want:    0,
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("GetCount", tt.args.ctx).Return(tt.want, nil)

			_m := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			got, err := _m.GetCount(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCount() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if got != tt.want {
				t.Errorf("GetCount() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestOrderer_RemoveOldOrders(t *testing.T) {
	type fields struct {
		storage       *storage.MockOrderStorager
		allowedZone   *geo.MockPolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				storage:       storage.NewMockOrderStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("RemoveOldOrders", tt.args.ctx, mock.Anything).Return(nil)

			_m := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}
			if err := _m.RemoveOldOrders(tt.args.ctx); (err != nil) != tt.wantErr {
				t.Errorf("RemoveOldOrders() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}

func TestOrderer_Save(t *testing.T) {
	type fields struct {
		storage       *storage.MockOrderStorager
		allowedZone   *geo.MockPolygonChecker
		disabledZones []geo.PolygonChecker
	}
	type args struct {
		ctx   context.Context
		order models.Order
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				storage:       storage.NewMockOrderStorager(t),
				allowedZone:   geo.NewPolygonChecker(t),
				disabledZones: nil,
			},
			args: args{
				ctx:   context.Background(),
				order: models.Order{},
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.storage.On("Save", tt.args.ctx, tt.args.order, mock.Anything).Return(nil)

			_m := &OrderService{
				storage:       tt.fields.storage,
				allowedZone:   tt.fields.allowedZone,
				disabledZones: tt.fields.disabledZones,
			}

			if err := _m.Save(tt.args.ctx, tt.args.order); (err != nil) != tt.wantErr {
				t.Errorf("Save() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
