package storage

import (
	"context"
	"encoding/json"
	"fmt"
	"github.com/redis/go-redis/v9"
	"gitlab.com/GoGerman/geotask/module/order/models"
	"log"
	"strconv"
	"strings"
	"time"
)

//go:generate mockery --name OrderStorager

type OrderStorager interface {
	Save(ctx context.Context, order models.Order, maxAge time.Duration) error                       // сохранить заказ с временем жизни
	GetByID(ctx context.Context, orderID int) (*models.Order, error)                                // получить заказ по id
	GenerateUniqueID(ctx context.Context) (int64, error)                                            // сгенерировать уникальный id
	GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) // получить заказы в радиусе от точки
	getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error)
	GetCount(ctx context.Context) (int, error)                       // получить количество заказов
	RemoveOldOrders(ctx context.Context, maxAge time.Duration) error // удалить старые заказы по истечению времени maxAge
}

type OrderStorage struct {
	storage *redis.Client
}

func NewOrderStorage(storage *redis.Client) OrderStorager {
	return &OrderStorage{storage: storage}
}

func (o *OrderStorage) Save(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// save with geo redis
	return o.saveOrderWithGeo(ctx, order, maxAge)
}

func (o *OrderStorage) RemoveOldOrders(ctx context.Context, maxAge time.Duration) error {
	// получить ID всех старых ордеров, которые нужно удалить
	// используя метод ZRangeByScore
	// старые ордеры это те, которые были созданы две минуты назад
	// и более

	// Проверить количество старых ордеров
	// удалить старые ордеры из redis используя метод ZRemRangeByScore где ключ "orders" min "-inf" max "(время создания старого ордера)"
	// удалять ордера по ключу не нужно, они будут удалены автоматически по истечению времени жизни
	oldOrders, err := o.storage.ZRangeByScore(ctx, "orders_zSet",
		&redis.ZRangeBy{
			Max: fmt.Sprintf("%d", time.Now().Add(-maxAge).Unix()),
			Min: "0",
		}).Result()
	if err != nil {
		log.Println("ошибка в ZRangeByScore:", err)
		return err
	}

	if len(oldOrders) == 0 {
		return nil
	}

	_, err = o.storage.ZRemRangeByScore(ctx, "orders_zSet", "-inf", fmt.Sprintf("%d", time.Now().Add(-maxAge).Unix())).Result()
	if err != nil {
		log.Println("ошибка в ZRemRangeByScore:", err)
		return err
	}
	return nil
}

func (o *OrderStorage) GetByID(ctx context.Context, orderID int) (*models.Order, error) {
	var order models.Order
	// получаем ордер из redis по ключу order:ID
	key := fmt.Sprintf("order:%d", orderID)
	res, err := o.storage.Get(ctx, key).Bytes()
	// проверяем что ордер не найден исключение redis.Nil, в этом случае возвращаем nil, nil
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}
	// десериализуем ордер из json
	err = json.Unmarshal(res, &order)
	if err != nil {
		return nil, err
	}

	return &order, nil
}

func (o *OrderStorage) saveOrderWithGeo(ctx context.Context, order models.Order, maxAge time.Duration) error {
	// сериализуем ордер в json
	data, err := json.Marshal(order)
	if err != nil {
		return err
	}
	// сохраняем ордер в json redis по ключу order:ID с временем жизни maxAge
	key := fmt.Sprintf("order:%d", order.ID)
	err = o.storage.Set(ctx, key, data, maxAge).Err()
	if err != nil {
		return err
	}
	// добавляем ордер в гео индекс используя метод GeoAdd где Name - это ключ ордера, а Longitude и Latitude - координаты
	err = o.storage.GeoAdd(ctx, "orders_location", &redis.GeoLocation{
		Name:      key,
		Longitude: order.Lng,
		Latitude:  order.Lat,
	}).Err()
	if err != nil {
		return err
	}
	// zset сохраняем ордер для получения количества заказов со сложностью O(1)
	// Score - время создания ордера
	err = o.storage.ZAdd(ctx, "orders_zSet", redis.Z{
		Score:  float64(order.CreatedAt.Unix()),
		Member: key,
	}).Err()

	return err
}

func (o *OrderStorage) GetCount(ctx context.Context) (int, error) {
	// получить количество ордеров в упорядоченном множестве используя метод ZCard
	count, err := o.storage.ZCard(ctx, "orders_zSet").Result()
	if err != nil {
		return 0, err
	}
	return int(count), nil
}

func (o *OrderStorage) GetByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]models.Order, error) {
	// используем метод getOrdersByRadius для получения ID заказов в радиусе
	ordersLocation, err := o.getOrdersByRadius(ctx, lng, lat, radius, unit)
	// обратите внимание, что в случае отсутствия заказов в радиусе
	// метод getOrdersByRadius должен вернуть nil, nil (при ошибке redis.Nil)
	if err != nil {
		return nil, err
	}
	if err == redis.Nil {
		return nil, nil
	}

	var orders []models.Order
	// проходим по списку ID заказов и получаем данные о заказе
	// получаем данные о заказе по ID из redis по ключу order:ID
	for i := range ordersLocation {
		num, err := strconv.Atoi(strings.Split(ordersLocation[i].Name, ":")[1])
		if err != nil {
			log.Println(err)
			return nil, err
		}
		data, _ := o.GetByID(ctx, num)
		if err != nil {
			log.Println(err)
			return nil, err
		}
		if data != nil {
			orders = append(orders, *data)
		}
	}

	return orders, nil
}

func (o *OrderStorage) getOrdersByRadius(ctx context.Context, lng, lat, radius float64, unit string) ([]redis.GeoLocation, error) {
	// в данном методе мы получаем список ордеров в радиусе от точки
	// возвращаем список ордеров с координатами и расстоянием до точки
	query := &redis.GeoRadiusQuery{
		Radius:      radius,
		Unit:        unit,
		WithCoord:   true,
		WithDist:    true,
		WithGeoHash: true,
	}

	orders, err := o.storage.GeoRadius(ctx, "orders_location", lng, lat, query).Result()
	if err != nil {
		return nil, err
	}
	return orders, nil
}

func (o *OrderStorage) GenerateUniqueID(ctx context.Context) (int64, error) {
	// генерируем уникальный ID для ордера
	// используем для этого redis incr по ключу order:id
	val, err := o.storage.Incr(ctx, "order:id").Result()
	if err != nil {
		return 0, err
	}

	return val, nil
}
