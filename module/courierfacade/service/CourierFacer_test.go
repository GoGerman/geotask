package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	cmodels "gitlab.com/GoGerman/geotask/module/courier/models"
	cservice "gitlab.com/GoGerman/geotask/module/courier/service"
	"gitlab.com/GoGerman/geotask/module/courierfacade/models"
	omodels "gitlab.com/GoGerman/geotask/module/order/models"
	oservice "gitlab.com/GoGerman/geotask/module/order/service"
	"reflect"
	"testing"
)

func TestCourierFacer_GetStatus(t *testing.T) {
	type fields struct {
		courierService *cservice.MockCourierer
		orderService   *oservice.MockOrderer
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   models.CourierStatus
	}{
		{
			name: "test1",
			fields: fields{
				courierService: cservice.NewMockCourierer(t),
				orderService:   oservice.NewMockOrderer(t),
			},
			args: args{
				ctx: context.Background(),
			},
			want: models.CourierStatus{
				Courier: cmodels.Courier{},
				Orders:  []omodels.Order{},
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&cmodels.Courier{}, nil)
			tt.fields.orderService.On("GetByRadius", tt.args.ctx, 0.0, 0.0, mock.Anything, "m").Return([]omodels.Order{}, nil)

			_m := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			if got := _m.GetStatus(tt.args.ctx); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetStatus() = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierFacer_MoveCourier(t *testing.T) {
	type fields struct {
		courierService *cservice.MockCourierer
		orderService   *oservice.MockOrderer
	}
	type args struct {
		ctx       context.Context
		direction int
		zoom      int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "test1",
			fields: fields{
				courierService: cservice.NewMockCourierer(t),
				orderService:   oservice.NewMockOrderer(t),
			},
			args: args{
				ctx:       context.Background(),
				direction: 0,
				zoom:      0,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierService.On("GetCourier", tt.args.ctx).Return(&cmodels.Courier{}, nil)
			tt.fields.courierService.On("MoveCourier", cmodels.Courier{}, tt.args.direction, tt.args.zoom).Return(nil)

			_m := &CourierFacade{
				courierService: tt.fields.courierService,
				orderService:   tt.fields.orderService,
			}
			_m.MoveCourier(tt.args.ctx, tt.args.direction, tt.args.zoom)
		})
	}
}
