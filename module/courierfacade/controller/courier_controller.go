package controller

import (
	"context"
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/GoGerman/geotask/module/courierfacade/service"
	"log"
	"net/http"
	"time"
)

type CourierController struct {
	courierService service.CourierFacer
}

func NewCourierController(courierService service.CourierFacer) *CourierController {
	return &CourierController{courierService: courierService}
}

func (c *CourierController) GetStatus(ctx *gin.Context) {
	// установить задержку в 50 миллисекунд
	time.Sleep(50 * time.Millisecond)
	// получить статус курьера из сервиса courierService используя метод GetStatus
	// отправить статус курьера в ответ
	status := c.courierService.GetStatus(ctx)
	// Отправить статус курьера в ответ
	//ctx.JSON(http.StatusOK, gin.H{"status": status})
	ctx.JSON(http.StatusOK, status)
}

func (c *CourierController) MoveCourier(m webSocketMessage) {
	var cm CourierMove
	// получить данные из m.Data и десериализовать их в структуру CourierMove
	err := json.Unmarshal((m.Data).([]byte), &cm)
	if err != nil {
		log.Println(err)
		return
	}
	// вызвать метод MoveCourier у courierService
	c.courierService.MoveCourier(context.Background(), cm.Direction, cm.Zoom)
}
