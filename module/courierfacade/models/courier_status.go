package models

import (
	cm "gitlab.com/GoGerman/geotask/module/courier/models"
	om "gitlab.com/GoGerman/geotask/module/order/models"
)

type CourierStatus struct {
	Courier cm.Courier `json:"courier"`
	Orders  []om.Order `json:"orders"`
}
