package storage

import (
	"context"
	"encoding/json"
	"github.com/redis/go-redis/v9"
	"gitlab.com/GoGerman/geotask/module/courier/models"
)

//go:generate mockery --name CourierStorager

type CourierStorager interface {
	Save(ctx context.Context, courier models.Courier) error // сохранить курьера по ключу courier
	GetOne(ctx context.Context) (*models.Courier, error)    // получить курьера по ключу courier
}

type CourierStorage struct {
	storage *redis.Client
}

func NewCourierStorage(storage *redis.Client) CourierStorager {
	return &CourierStorage{storage: storage}
}

func (c *CourierStorage) Save(ctx context.Context, courier models.Courier) error {
	courierJSON, err := json.Marshal(courier)
	if err != nil {
		return err
	}

	err = c.storage.Set(ctx, "courier", courierJSON, 0).Err()
	if err != nil {
		return err
	}

	return nil
}

func (c *CourierStorage) GetOne(ctx context.Context) (*models.Courier, error) {
	courierJSON, err := c.storage.Get(ctx, "courier").Bytes()
	if err != nil {
		if err == redis.Nil {
			return nil, nil
		}
		return nil, err
	}

	var courier models.Courier
	err = json.Unmarshal(courierJSON, &courier)
	if err != nil {
		return nil, err
	}

	return &courier, nil
}
