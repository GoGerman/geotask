package service

import (
	"context"
	"github.com/stretchr/testify/mock"
	"gitlab.com/GoGerman/geotask/geo"
	"gitlab.com/GoGerman/geotask/module/courier/models"
	"gitlab.com/GoGerman/geotask/module/courier/storage"
	"reflect"
	"testing"
)

func TestCourierer_GetCourier(t *testing.T) {
	type fields struct {
		courierStorage *storage.MockCourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *models.Courier
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				courierStorage: storage.NewCourierStorager(t),
				allowedZone:    geo.NewPolygonChecker(t),
				disabledZones:  nil,
			},
			args: args{
				ctx: context.Background(),
			},
			want: &models.Courier{
				Score: 0,
				Location: models.Point(geo.Point{
					Lat: DefaultCourierLat,
					Lng: DefaultCourierLng,
				}),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("GetOne", mock.Anything).Return(tt.want, nil)
			tt.fields.courierStorage.On("Save", mock.Anything, mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			_m := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			got, err := _m.GetCourier(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetCourier() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("GetCourier() got = %v, want %v", got, tt.want)
			}
		})
	}
}

func TestCourierer_MoveCourier(t *testing.T) {
	type fields struct {
		courierStorage *storage.MockCourierStorager
		allowedZone    *geo.MockPolygonChecker
		disabledZones  []geo.PolygonChecker
	}
	type args struct {
		courier   models.Courier
		direction int
		zoom      int
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "test1",
			fields: fields{
				courierStorage: storage.NewCourierStorager(t),
				allowedZone:    geo.NewPolygonChecker(t),
				disabledZones:  nil,
			},
			args: args{
				courier:   models.Courier{},
				direction: 1,
				zoom:      1,
			},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			tt.fields.courierStorage.On("Save", mock.Anything, mock.Anything).Return(nil)
			tt.fields.allowedZone.On("Contains", mock.Anything).Return(true)

			_m := &CourierService{
				courierStorage: tt.fields.courierStorage,
				allowedZone:    tt.fields.allowedZone,
				disabledZones:  tt.fields.disabledZones,
			}
			if err := _m.MoveCourier(tt.args.courier, tt.args.direction, tt.args.zoom); (err != nil) != tt.wantErr {
				t.Errorf("MoveCourier() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
